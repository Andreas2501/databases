<Query Kind="Program" />

void Main()
{
	XDocument inhalt = XDocument.Load (@"D:\PC\Vortrag\results.xml");
	
	var rs=inhalt.Root.Element("test").Descendants("test").
				SelectMany(x => x.Descendants("verification")).
					Where(w => w.Element("result").Attribute ("type").Value == "FAIL").
				Select (a => new Fehler (a.Element("result").Attribute ("order").Value,
				String.Join(String.Empty,a.Element("result").Descendants ("description").Select (b => b.Value).ToList ()) ));
	
	var groupedAndOrdered=rs.GroupBy(g => g.orderName).
				Select(x => new Tuple<string,int>(x.Key,x.Aggregate(new HashSet<string>(), (a,b) => addMe(a,b.fehler)).Count)).
					OrderBy(y => y.Item2);
		
	groupedAndOrdered.Dump();
}

public HashSet<string> addMe (HashSet<string> aset, string fehler)
		{
			aset.Add(fehler);
			return aset;
		}

public class Fehler{
			public string orderName;
			public string fehler;

			public Fehler(string orderName, string fehler){
				this.orderName=orderName;
				this.fehler=fehler;
			}					
}