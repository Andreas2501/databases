using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using LinqToExcel;

namespace LinqExamples
{

	//http://www.albahari.com/nutshell/10linqmyths.aspx
	//http://weblogs.asp.net/scottgu/archive/2007/08/07/using-linq-to-xml-and-how-to-build-a-custom-rss-feed-reader-with-it.aspx
	//TODO excel assemblies entfernen
	class MainClass
	{
				
		public class Fehler{
			public string orderName;
			public string fehler;

			public Fehler(string orderName, string fehler){
				this.orderName=orderName;
				this.fehler=fehler;
			}					
}		
			
		public void xmlExample2 ()
		{
			XDocument inhalt = XDocument.Load (@"D:\PC\Vortrag\results_fpV2.xml");
			Console.WriteLine ("## C# ");

			var rs=inhalt.Root.Element("test").Descendants("test").
				SelectMany(x => x.Descendants("verification")).
					Where(w => w.Element("result").Attribute ("type").Value == "FAIL").
				Select (a => new Fehler (a.Element("result").Attribute ("order").Value,
				String.Join(String.Empty,a.Element("result").Descendants ("description").Select (b => b.Value).ToList ()) ));
	
	        var groupedAndOrdered=rs.GroupBy(g => g.orderName).
				Select(x => new Tuple<string,int>(x.Key,x.Aggregate(new HashSet<string>(), (a,b) => addMe(a,b.fehler)).Count)).
					OrderBy(y => y.Item2);
			
			foreach(var go in groupedAndOrdered){
				Console.WriteLine(" Fehler "+go.Item1+" "+go.Item2);
			}
			
			Console.WriteLine("ss "+rs.ToList().Count);
		}
		
		public HashSet<string> addMe (HashSet<string> aset, string fehler)
		{
			aset.Add(fehler);
			return aset;
		}

		

		public static void Main (string[] args)
		{
			//new MainClass().excelExample();
			new MainClass().xmlExample2();

		}

		

	}
}
