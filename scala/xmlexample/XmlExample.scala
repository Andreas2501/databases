package xmlexample

import scala.xml.Node

object XmlExample {

   case class Fehler(orderName:String, fehler:String)
          
       def main(args: Array[String]): Unit = {
         
         lazy val ergebnisse = scala.xml.XML.loadFile("results.xml")
     
         val r = (ergebnisse \\ "test").flatMap {
           x =>        
             (x \ "verification").flatMap {
               y =>
                 (y \ "result").flatMap {
                   z =>
                     if ((z \ "@type").text == "FAIL") {
                       val orderName = (z \ "@order").text
                       val errorDescribtions = z \ "description"
                       
                        Some(Fehler(orderName, errorDescribtions.map(a => a.text).mkString))
                     } else
                        None
                 }
             }
         }
                                 
         val question2=r.toSet[Fehler].groupBy(x => x.orderName).map{ case(a,b) => (a,b.size)}
          
         println("##scala")
          
         question2.iterator.toList.sortBy(x => x._2).foreach{ case(a,b) =>  println("order "+a+" "+b) }
  }

}