package simple;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;

public class XmlExample {

   private class Fehler{
		public final  String orderName;
		public final String fehler;
		
		public Fehler(String orderName, String fehler) {
			this.orderName = orderName;
			this.fehler = fehler;
		}
	}

	private void xml(String filePath) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException{
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory
				.newInstance();
		
		Document document = builderFactory.newDocumentBuilder().parse(new FileInputStream(filePath));

		// squishreport
		NodeList nodes = document.getChildNodes().item(0).getChildNodes().item(1).getChildNodes();

		ArrayList<T2<String,Integer>> ergebnisse=Lists.newArrayList();

		System.out.println("##java");
		for (Element test : find(nodes, new PredicateByName("test"))) {

			List<Element>  verifications=find(test.getChildNodes(), new PredicateByName("verification"));

			Iterable<Fehler> gefundeneFehler=Optional.presentInstances(Iterables.transform(verifications, new FilterAgainAndMap()));

			ImmutableMap<String, Collection<Fehler>> gruppiert=
					Multimaps.index(gefundeneFehler, new GroupFunction()).asMap();

			ergebnisse.addAll(Lists.newArrayList(Iterables.transform(gruppiert.entrySet(), new SortF())));
		}

		List<T2<String,Integer>> s2 = Ordering.from(byFehlerZahl).sortedCopy(ergebnisse);

		for (T2<String,Integer> t2 : s2) {
			System.out.println("order "+t2.a+" "+t2.b);
		}
	}

	Comparator<T2<String,Integer>> byFehlerZahl = new Comparator<T2<String,Integer>>() {

		@Override
		public int compare(T2<String,Integer> o1, T2<String,Integer> o2) {
			return o1.b.compareTo(o2.b);
		}
	};

	private class SortF implements Function<Entry<String, Collection<Fehler>>, T2<String,Integer>>{

		@Override
		public T2<String,Integer> apply(Entry<String, Collection<Fehler>> arg0) {
			Set<String> fehler=new HashSet<String>();
			for (Fehler f : arg0.getValue()) {
				fehler.add(f.fehler);
			}
			
			return new T2<String, Integer>(arg0.getKey(),fehler.size());
		}

	}

	private class GroupFunction implements Function<Fehler, String>{

		@Override
		public String apply(Fehler arg0) {
			return arg0.orderName;
		}

	}

	private class FilterAgainAndMap implements Function<Element,Optional<Fehler>> {

		@Override
		public Optional<Fehler> apply(Element arg0) {
			 List<Element>el=find(arg0.getChildNodes(),new PredicateByName("result"));

			 for (Element element : el) {
				if(element.getAttribute("type").equals("FAIL")){
					List<String>  fehlerListE=poormap(element.getChildNodes(), new TextContentToString());
					StringBuilder sb=new StringBuilder();
					
					for (String string : fehlerListE) {
						sb.append(string);
					}

					return Optional.of(new Fehler(element.getAttribute("order"),sb.toString()));
				}

			}

			return Optional.absent();
		}
	}

	private class TextContentToString implements Function<Element, String>{

		@Override
		public String apply(Element arg0) {
			return arg0.getTextContent();
		}

	}

	private class PredicateByName implements Predicate<Element> {

		private final String name;

		public PredicateByName(String name) {
			this.name = name;
		}
		@Override
		public boolean apply(Element arg0) {
			return arg0.getNodeName().equals(name);
		}
	}

	private <A>List<A> poormap(NodeList nodes, Function<Element, A> f){

		List<A> result=new ArrayList<A>();

		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);

			if (node instanceof Element) {
				Element child = (Element) node;

			    result.add(f.apply(child));

			}
		}
		return result;
	}

	// excercise: express @find with @poormap
	private List<Element> find(NodeList nodes, Predicate<Element> p){

		List<Element> result=new ArrayList<Element>();

		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);

			if (node instanceof Element) {
				Element child = (Element) node;

				if(p.apply(child))
					result.add(child);
			}
		}
		return result;
	}

	public class T2<A,B> {

		public final A a;
		public final B b;

		public T2(A a, B b) {
			this.a = a;
			this.b = b;
		}

		@Override
		public String toString() {
			return "t2 [a=" + a + ", b=" + b + "]";
		}
	}

	public static void main(String[] args) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException {
		   new XmlExample().xml(args[0]);

	}

}
