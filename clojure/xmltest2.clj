(ns xmltest2)
;;(use 'clojure.xml) 
;;Parse XML Funktionen
;; siehe seine verwendung von 'some'
;; !!clojure is lazy by default!!
(defn named [k] (if (= (:content k) nil) "nix" (:content k)))

;;y-> result
;;     z-> description
(defn get-result-descriptions [y] (map (fn [z] (if (= (map :type (map :attrs z)) nil)
                                                  "empty" 
                                                  (apply str (:content z)))) (:content y)))
;; x-> verfication
;;   y-> result
(defn handle-veri3 [x] 
  (if  (=(:tag x) :verification)  
       (map (fn [y] 
                   (if (and 
                       (= (:tag y) :result)
                       (= (:type (:attrs y)) "FAIL"))   
                         {:ordername (str (:order (:attrs  y))) 
                          :error (apply str (get-result-descriptions y))} [] )
               ) (:content x))
       [] ))

(defn name-count2 [x] {:count (count (set (val x))) :name (key x)} )

;;results_fpV2.xml
(map println (sort-by :count (map name-count2 (group-by :ordername (flatten (map handle-veri3
              (flatten (map named (flatten (map :content
                                                (:content (parse (java.io.FileInputStream. "D:\\PC\\Vortrag\\results.xml"))))))))))))))