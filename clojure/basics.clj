(ns basics)

;; Map syntax
(get {:a 1, :b 2} :e 0) 
 
 ;; (use 'clojure.data.xml)
 ;; key/val für mapentrys und 'name' für KeyWord
  
;;2 ToplevelNodes: SquishReport und test , es muss einfacher mit tree-seq gehen!!
 ;; Beispiel Funktionen
(:content (parse (java.io.FileInputStream. "D:\\PC\\Vortrag\\simple.xml")))

;; display the type of an expression
(type [1 2 3])

(filter (fn [x] (> (val x) 100))
	       {:a 1 :b 2 :c 101 :d 102 :e -1})

(filter (fn [m] (= (= (:verification (:tag m)) nil) false))
        
;;small data
{:tag :result, :attrs {:type "FAIL", :time "2012-09-07T23:43:07+02:00", :order "AIP -> AIP-Account anlegen (PK)"}}

;;finally xml BLA to STRING
(apply str (get-contents2 (element :lvl1 {} (element :lvl2 {:type "a"} "GEFUNDEN"))))
;; (use 'clojure.set)
(apply union (map set [[1 2 3][1 1 2 3][2 3 4 5]]))

(map (fn [x](str (count (val x)) "-" (key x))) {"A1" [{:ordername "A1", :error "fehler1"} {:ordername "A1", :error "fehler2"}],
                     "A2"[{:ordername "A2", :error "f1"}]}   )
